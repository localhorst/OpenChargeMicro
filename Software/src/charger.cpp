/*
 * Charger.cpp
 *
 *  Created on: 19.11.2018
 *      Author: Hendrik Schutter
 */

#include "openChargeMicro.h"

charger::charger(const struct s_charger pCharger) {
	charger_settings = pCharger;
	status.connected = false;
	status.active = false;
	//active = false;
	capacity = 0;
}

charger::~charger() {
}

/* returns current charge-current in mA */
int charger::getCurrent() {
	mux.setChannel(charger_settings.chI);
	int tmp = io.readAdc(0);
	int ret = (int) ((tmp * 1.4286) + 34.857); //TODO calibration and testing
	return ret;
}

/* returns current battery voltage in V */
double charger::getVoltage() {
	mux.setChannel(charger_settings.chU);
	int tmp = io.readAdc(0);
	if (tmp == 0)
		return 0;
	double ret = ((double) tmp) * 0.00615;
	return ret;
}

/* returns summed up charged capacity since charge start in µAh */
unsigned int charger::getCapacity() {
	return capacity;
}

void charger::setStartTime(struct time_t pTime) {
	startTime = pTime;
}
struct time_t charger::getStartTime() {
	return startTime;
}

void charger::getInfo() {
#ifdef DEBUG
	char buffer[50];
	sprintf(buffer, "Nr: %i - Uch %i - Ich %i\r\n", charger_settings.nr,
			charger_settings.chU, charger_settings.chI);
	serialSend(buffer);
#endif
}
void charger::setStatus(struct s_charger_status pStatus) {
	status = pStatus;
}
struct s_charger_status charger::getStatus() {
	return status;
}

/* resets the capacity */
void charger::reset() {
	capacity = 0;
}

/* updates the capacity */
void charger::update() {

	static int full;

	if (getCurrent() < 36) {
		//not charging --> maybe battery full

		full++;

		if (full == 5) {
			//not charging --> battery full
			status.active = false;
			full = 0;
#ifdef DEBUG
			serialSend("battery full\r\n");
#endif
		}

	} else {
		//not full --> charging
		// 1sec / 3600
		const double div = 0.000277778;
		capacity = capacity + ((unsigned long int) (div * getCurrent() * 1000));
		//serialSend("update\r\n");

		full = 0;
	}
}

