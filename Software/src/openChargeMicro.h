
//#define DEBUG


/* system header */
#include <avr/io.h>
#include <string.h>
#include <util/delay.h>
#include <avr/interrupt.h>
#include <stdio.h>
#include <stdlib.h>

/* project header */
#include "ws2812/ws2812.h"
#include "ioController.h"
#include "multiplexer.h"

#include "clock.h"
#include "gui.h"
#include "charger.h"

/* Pins */
#define WS2812B PD2
#define ACTIVELED PB5
#define PROBE15V PC1
#define PROBE5V PC2
#define Z PC0
#define VTX PB3
#define VRX PB2
#define BUZZER PB1
#define POWERON PB0
#define S2 PD7
#define S1 PD6
#define S0 PD5

#ifdef DEBUG
/* Serial */
#define BUAD 9600
#define BUAD_RATE_CALC ((F_CPU/16/BUAD) - 1)


void serialSetup(void);
void serialSend(const char* sendString);

#endif
/* Charger Config */
#define CHARGER_SIZE 4
#define CONNECTION_TIMEOUT 3

// Charger 0
#define CH0_NR 0
#define CH0_U 0
#define CH0_I 4

// Charger 1
#define CH1_NR 1
#define CH1_U 1
#define CH1_I 5

// Charger 2
#define CH2_NR 2
#define CH2_U 2
#define CH2_I 6

// Charger 3
#define CH3_NR 3
#define CH3_U 3
#define CH3_I 7

/* ws2812 */
#define LED_C 1
#define BRIGHTNESS 255 //0 to 255

void updateChargers();

void updateGUI();

void checkForBattery();




