/*
 * gui.h
 *
 *  Created on: 25.11.2018
 *      Author: Hendrik Schutter
 */

#include "oled_ssd1306/oled_ssd1306.h"

#ifndef GUI_H_
#define GUI_H_


class gui{
private:

	oled_ssd1306 oled;

public:
	gui();
	~gui();
	void gui_init();
	void gui_print(int pNr, bool pStatus, struct time_t pTime, double pVoltage, int pCurrent, unsigned long int pCapacity);
	void gui_write(char* str);
	void gui_info();

};

#endif /* GUI_H_ */
