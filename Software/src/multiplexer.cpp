/*
 * multiplexer.cpp
 *
 *  Created on: 20.11.2018
 *      Author: Hendrik Schutter
 */

#include "openChargeMicro.h"

multiplexer::multiplexer() {

}
multiplexer::~multiplexer() {

}
void multiplexer::setChannel(int pCh) {
// see http://www.ti.com/lit/ds/symlink/cd4051b.pdf
	switch (pCh) {
	case 0:
		io.setMultiplexer(0, 0, 0);
		break;
	case 1:
		io.setMultiplexer(0, 0, 1);
		break;
	case 2:
		io.setMultiplexer(0, 1, 0);
		break;
	case 3:
		io.setMultiplexer(0, 1, 1);
		break;
	case 4:
		io.setMultiplexer(1, 0, 0);
		break;
	case 5:
		io.setMultiplexer(1, 0, 1);
		break;
	case 6:
		io.setMultiplexer(1, 1, 0);
		break;
	case 7:
		io.setMultiplexer(1, 1, 1);
		break;

	default:
		io.setMultiplexer(0, 0, 0);
		break;
	}

}

