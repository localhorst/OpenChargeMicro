/*
 * gui.cpp
 *
 *  Created on: 25.11.2018
 *      Author: Hendrik Schutter
 */

#include "openChargeMicro.h"

gui::gui() {

}
gui::~gui() {

}

void gui::gui_init() {
	oled.oled_init();

	oled.oled_gotoxy(0, 1);
	oled.oled_write(__DATE__);

	oled.oled_font_size(2);
	oled.oled_gotoxy(0, 4);
	oled.oled_write("GPL 3.0");
	oled.oled_font_size(0);
}

void gui::gui_info() {
	oled.oled_clear_screen();
	oled.oled_gotoxy(0, 0);
	oled.oled_write("OpenChargeMicro");
	oled.oled_gotoxy(0, 2);
	oled.oled_write("HW: 0.1 SW: dev");
	oled.oled_gotoxy(0, 6);
	oled.oled_write("Akkus einstecken");
}

void gui::gui_print(int pNr, bool pStatus, struct time_t pTime, double pVoltage,
		int pCurrent, unsigned long int pCapacity) {

	oled.oled_clear_screen();

	char buffer[50];

	oled.oled_font_size(0);

	/* Number and Status */
	oled.oled_gotoxy(0, 0);

	if (pStatus) {
		//charging
		sprintf(buffer, "#%i - laedt", pNr);
	} else {
		//full
		sprintf(buffer, "#%i - voll", pNr);
	}
	oled.oled_write_str(buffer);

	/* time */
	oled.oled_gotoxy(0, 2);

	sprintf(buffer, "----%02d:%02d:%02d----", pTime.diffHour, pTime.diffMinute,
			pTime.diffSecond);

	oled.oled_write_str(buffer);

	/* voltage and current */
	oled.oled_gotoxy(0, 4);

	char charVoltage[10];

	dtostrf(pVoltage, 1, 2, charVoltage);

	sprintf(buffer, "U:%sV I:%imA", charVoltage, pCurrent);
	oled.oled_write_str(buffer);

	/* capacity */
	oled.oled_gotoxy(0, 6);

	char charCapacity[10];

	if (pCapacity > 1000) {
		//mAh
		dtostrf((double) (pCapacity / 1000.0), 3, 2, charCapacity);
		sprintf(buffer, "C: %smAh", charCapacity);
	} else {
		//µAh
		//dtostrf(pCapacity, 1, 2, charCapacity);
		sprintf(buffer, "C: %iuAh", (int) pCapacity);
	}

	oled.oled_write_str(buffer);

}

void gui::gui_write(char* str) {
	oled.oled_clear_screen();
	oled.oled_gotoxy(0, 0);
	oled.oled_write_str(str);
}
