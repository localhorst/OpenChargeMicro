/*
 * charger.h
 *
 *  Created on: 20.11.2018
 *      Author: Hendrik Schutter
 */

#ifndef SRC_CHARGER_H_
#define SRC_CHARGER_H_

struct s_charger {
	int nr;
	int chU;
	int chI;
};

struct s_charger_status{
	bool connected;
	bool active;
};

class charger {

private:
	struct s_charger charger_settings;
	ioController io;
	multiplexer mux;
	struct time_t startTime;
	unsigned long int capacity; //in µAh
	struct s_charger_status status;

public:
	charger(const struct s_charger pCharger);
	~charger();
	int getCurrent();
	double getVoltage();
	unsigned int getCapacity();
	void setStartTime(struct time_t pTime);
	struct time_t getStartTime();
	void getInfo();
	void setStatus(struct s_charger_status pStatus);
	struct s_charger_status getStatus();
	void reset();
	void update();
};

#endif /* SRC_CHARGER_H_ */
