/*
 * ioController.h
 *
 *  Created on: 20.11.2018
 *      Author: Hendrik Schutter
 */

#ifndef SRC_IOCONTROLLER_H_
#define SRC_IOCONTROLLER_H_

class ioController {

private:

	void setWS2812(const unsigned char red, const unsigned char green,
			const unsigned char blue);
	void stopBuzzer();

public:
	ioController();
	~ioController();
	double get5VProbe();
	double get15VProbe();
	void ports_init();
	void activateChargers();
	void deactivateChargers();
	void setActiveLED(bool pBool);

	void adc_init(void);
	int readAdc(char chan);
	void setMultiplexer(bool pS2, bool pS1, bool pS0);

	void setWS2812_clear(void);
	void setWS2812_green(void);
	void setWS2812_red(void);

	void startBuzzer();

};

#endif /* SRC_IOCONTROLLER_H_ */
