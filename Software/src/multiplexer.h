/*
 * multiplexer.h
 *
 *  Created on: 20.11.2018
 *      Author: Hendrik Schutter
 */

#ifndef SRC_MULTIPLEXER_H_
#define SRC_MULTIPLEXER_H_

class multiplexer {

private:
	ioController io;

public:
	multiplexer();
	~multiplexer();
	void setChannel(int pCh);
};

#endif /* SRC_MULTIPLEXER_H_ */
