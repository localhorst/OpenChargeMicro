/*
 * ioController.cpp
 *
 *  Created on: 20.11.2018
 *      Author: Hendrik Schutter
 */

#include "openChargeMicro.h"

ioController::ioController() {
	ports_init();
	adc_init();
	//beeps = 0;
	PORTB |= (1 << BUZZER);
}

ioController::~ioController() {

}

double ioController::get5VProbe() {
	int tmp = (int) readAdc(2);
	double ret = ((double) tmp) * 0.006242;
	return ret;
}

double ioController::get15VProbe() {
	int tmp = (int) readAdc(1);
	double ret = ((double) tmp) * 0.014968;
	return ret;
}

void ioController::ports_init() {
	DDRB |= 1 << ACTIVELED;
	DDRB |= 1 << BUZZER;
	DDRB |= 1 << POWERON;
	DDRD |= 1 << S2;
	DDRD |= 1 << S1;
	DDRD |= 1 << S0;

	DDRC &= ~(1 << PROBE15V);
	DDRC &= ~(1 << PROBE5V);
	DDRC &= ~(1 << Z);
}

void ioController::activateChargers() {

	PORTB &= ~(1 << POWERON); //OFF
}

void ioController::deactivateChargers() {
	PORTB |= (1 << POWERON); //ON
}

void ioController::setActiveLED(bool pBool) {
	if (pBool) {
		PORTB |= (1 << ACTIVELED); //ON
	} else {
		PORTB &= ~(1 << ACTIVELED); //OFF
	}
}

void ioController::adc_init(void) {
	ADMUX = (1 << REFS0);     //select AVCC as reference
	ADCSRA = (1 << ADEN) | 7;  //enable and prescale = 128 (16MHz/128 = 125kHz)
}

int ioController::readAdc(char ch) {
	ADMUX = (1 << REFS0) | (ch & 0x0f);  //select input and ref
	ADCSRA |= (1 << ADSC);                 //start the conversion
	while (ADCSRA & (1 << ADSC))
		;          //wait for end of conversion
	return ADCW;
}

void ioController::setMultiplexer(bool pS2, bool pS1, bool pS0) {

	if (pS0) {
		PORTD |= (1 << S0); //ON
	} else {
		PORTD &= ~(1 << S0); //OFF
	}

	if (pS1) {
		PORTD |= (1 << S1); //ON
	} else {
		PORTD &= ~(1 << S1); //OFF
	}

	if (pS2) {
		PORTD |= (1 << S2); //ON
	} else {
		PORTD &= ~(1 << S2); //OFF
	}

}

void ioController::setWS2812(const unsigned char red, const unsigned char green,
		const unsigned char blue) {

	WS2812 led(LED_C);
	led.set_output(&PORTD, &DDRD, DDD2);
	Color strobe;
	uint8_t i;

	strobe.r = red;
	strobe.g = green;
	strobe.b = blue;
	for (i = 0; i < LED_C; i++)
		led.set_rgb_at(i, strobe);
	led.sync();
}

void ioController::setWS2812_clear() {
	setWS2812(0, 0, 0);
}

void ioController::setWS2812_green() {
	setWS2812(0, BRIGHTNESS, 0);
}

void ioController::setWS2812_red() {
	setWS2812(BRIGHTNESS, 0, 0);
}

void ioController::startBuzzer() {
	//beeps = 0;

	//PORTB |= (1 << BUZZER);    // switch Buzzer on
	PORTB &= ~(1 << BUZZER);

	// Set the Timer Mode to CTC
	TCCR0A |= (1 << WGM01);

	// Set the value that you want to count to
	OCR0A = 0xFF;

	TIMSK0 |= (1 << OCIE0A);    //Set the ISR COMPA vect

	TCCR0B |= (1 << CS02) | (1 << CS00);
	// set prescaler to 1024 and start the timer

	sei();
	//enable interrupts

}

void ioController::stopBuzzer() {
/*
	if (beeps < 100) {

		if (beeps % 10 == 0) {
			PORTB ^= (1 << BUZZER);

		}

		beeps++;

	} else {
		//PORTB &= ~(1 << BUZZER);    // switch Buzzer off
		PORTB |= (1 << BUZZER);
		TCCR0B = 0; //stop timer
		beeps = 0;
	}
*/
}

