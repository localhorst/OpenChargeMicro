/*
 * clock.h
 *
 *  Created on: 22.11.2018
 *      Author: Hendrik Schutter
 */

#ifndef SRC_CLOCK_H_
#define SRC_CLOCK_H_

struct time_t {
	int diffHour;
	int diffMinute;
	int diffSecond;
	unsigned long int seconds;
};

class clock {

private:
	void clock_init();

public:
	clock();
	~clock();
	unsigned long int getTime(); //time in sec since boot
	struct time_t getTime(struct time_t pTime); //time in sec since time stamp
	struct time_t getTimeStamp(); // returns a timestamp

};

#endif /* SRC_CLOCK_H_ */
