# OpenChargeMicro

4 channel 1S LiPo Charger with Display.


## Overview

![](https://git.mosad.xyz/localhorst/OpenChargeMicro/raw/commit/d075ca521c97151ab1d812794867f897e43c5ede/Hardware/OpenChargeMicro/Top.png)

* 4 separated chargers
* max 1A chargingcurrent per charger
* power via usb or dc plug (8V - 15V)
* Monitoring voltage, current and time
* Buzzer and WS2812B for audiovisual alerts
* Battery voltage detections (low battery warning)
* Expansion slot for Bluetooth module
* Free Hardware and Software --> GPL3.0

## Hardware


todo


## Software

###Libraries

https://github.com/marenz2569/libws2812_avr

https://www.ulrichradig.de/home/index.php/avr/oled-display-ssd1306
todo


